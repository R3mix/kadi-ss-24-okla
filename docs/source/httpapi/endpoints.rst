.. _httpapi-endpoints:

Endpoints
---------

The following sections consist of the documentation of the actual API endpoints
themselves. The sections are separated by API version, while each section is itself
separated by the different resources that the API provides access to.

.. warning::
    As long as |kadi| is still in beta, API endpoints and/or resource representations
    are still subject to change without a guarantee of prior deprecation.

The documentation of each endpoint additionally includes its versions, as the
functionality of an endpoint might not have changed between different API versions,
required access token scopes, query parameters and/or request data as well as status
codes specific to the endpoint. If not otherwise noted, the common request/response
formats and status codes, as explained in the :ref:`general <httpapi-general>` section,
apply for each of the documented endpoints.

.. toctree::
    :maxdepth: 2

    latest/index
