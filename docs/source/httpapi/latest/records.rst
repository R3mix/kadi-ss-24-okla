Records
=======

.. include:: ../snippets/records.rst

GET
---

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.records.api
    :methods: get
    :autoquickref:

POST
----

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.records.api
    :methods: post
    :autoquickref:
    :undoc-endpoints: api.upload_file_legacy

PATCH
-----

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.records.api
    :methods: patch
    :autoquickref:

PUT
---

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.records.api
    :methods: put
    :autoquickref:

DELETE
------

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.records.api
    :methods: delete
    :autoquickref:
