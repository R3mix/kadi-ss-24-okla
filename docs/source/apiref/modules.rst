.. _apiref-modules:

Modules
=======

This section contains a complete API reference of the :mod:`kadi.modules` Python module.

Accounts
--------

.. automodule:: kadi.modules.accounts.core

.. automodule:: kadi.modules.accounts.forms

.. automodule:: kadi.modules.accounts.models

.. automodule:: kadi.modules.accounts.providers.core

.. automodule:: kadi.modules.accounts.providers.ldap

.. automodule:: kadi.modules.accounts.providers.local

.. automodule:: kadi.modules.accounts.providers.oidc

.. automodule:: kadi.modules.accounts.providers.shib

.. automodule:: kadi.modules.accounts.schemas

.. automodule:: kadi.modules.accounts.tasks

.. automodule:: kadi.modules.accounts.utils

Collections
-----------

.. automodule:: kadi.modules.collections.core

.. automodule:: kadi.modules.collections.export

.. automodule:: kadi.modules.collections.forms

.. automodule:: kadi.modules.collections.mappings

.. automodule:: kadi.modules.collections.models

.. automodule:: kadi.modules.collections.schemas

.. automodule:: kadi.modules.collections.utils

Groups
------

.. automodule:: kadi.modules.groups.core

.. automodule:: kadi.modules.groups.forms

.. automodule:: kadi.modules.groups.mappings

.. automodule:: kadi.modules.groups.models

.. automodule:: kadi.modules.groups.schemas

.. automodule:: kadi.modules.groups.utils

Main
----

.. automodule:: kadi.modules.main.forms

.. automodule:: kadi.modules.main.schemas

.. automodule:: kadi.modules.main.tasks

.. automodule:: kadi.modules.main.utils

Records
-------

.. automodule:: kadi.modules.records.core

.. automodule:: kadi.modules.records.export

.. automodule:: kadi.modules.records.extras

.. automodule:: kadi.modules.records.files

.. automodule:: kadi.modules.records.forms

.. automodule:: kadi.modules.records.links

.. automodule:: kadi.modules.records.mappings

.. automodule:: kadi.modules.records.models

.. automodule:: kadi.modules.records.previews

.. automodule:: kadi.modules.records.schemas

.. automodule:: kadi.modules.records.tasks

.. automodule:: kadi.modules.records.uploads

.. automodule:: kadi.modules.records.utils

Settings
--------

.. automodule:: kadi.modules.settings.forms

.. automodule:: kadi.modules.settings.utils

Sysadmin
--------

.. automodule:: kadi.modules.sysadmin.forms

.. automodule:: kadi.modules.sysadmin.utils

Templates
---------

.. automodule:: kadi.modules.templates.core

.. automodule:: kadi.modules.templates.export

.. automodule:: kadi.modules.templates.forms

.. automodule:: kadi.modules.templates.mappings

.. automodule:: kadi.modules.templates.models

.. automodule:: kadi.modules.templates.schemas

.. automodule:: kadi.modules.templates.utils
