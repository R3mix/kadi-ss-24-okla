# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.security import decode_jwt
from kadi.lib.security import encode_jwt
from kadi.lib.security import hash_value


@pytest.mark.parametrize("hexadecimal", [True, False])
@pytest.mark.parametrize(
    "value,alg,is_valid",
    [("test", "invalid", False), ("test", None, True), (b"test", None, True)],
)
def test_hash_value(hexadecimal, value, alg, is_valid):
    """Test if values are hashed correctly."""
    alg = alg if alg is not None else "md5"

    result = hash_value(value, alg=alg, hexadecimal=hexadecimal)

    if is_valid:
        if hexadecimal:
            assert isinstance(result, str)
        else:
            assert isinstance(result, bytes)
    else:
        assert result is None


@pytest.mark.parametrize(
    "expires_in,is_valid", [(None, True), (1_000, True), (-1_000, False)]
)
def test_encode_decode_jwt(expires_in, is_valid):
    """Test if encoding and decoding JSON web tokens works correctly."""
    payload = {"test": "test"}
    jwt = encode_jwt(payload, expires_in=expires_in)

    if is_valid:
        assert decode_jwt(jwt) == payload
    else:
        assert decode_jwt(jwt) is None
