# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.exceptions import KadiPermissionError
from kadi.lib.permissions.core import add_role
from kadi.modules.records.core import update_record
from kadi.modules.records.links import create_record_link
from kadi.modules.records.links import create_record_links
from kadi.modules.records.links import get_linked_record
from kadi.modules.records.links import get_permitted_record_links
from kadi.modules.records.links import get_record_link_changes
from kadi.modules.records.links import get_record_links_graph
from kadi.modules.records.links import remove_record_link
from kadi.modules.records.links import update_record_link
from kadi.modules.records.models import RecordLink


def test_create_record_link(dummy_user, new_record, new_user):
    """Test if record links are created correctly."""
    record_1 = new_record()
    record_2 = new_record()

    name = "test"
    prev_timestamp_1 = record_1.last_modified
    prev_timestamp_2 = record_2.last_modified

    with pytest.raises(KadiPermissionError):
        create_record_link(
            name=name, record_from=record_1, record_to=record_2, creator=new_user()
        )

    with pytest.raises(ValueError) as e:
        create_record_link(
            name=name, record_from=record_1, record_to=record_1, creator=dummy_user
        )
        assert str(e) == "Cannot link record with itself."

    assert record_1.last_modified == prev_timestamp_1
    assert record_2.last_modified == prev_timestamp_2
    assert record_1.revisions.count() == 1
    assert record_2.revisions.count() == 1

    assert (
        create_record_link(
            name=name, record_from=record_1, record_to=record_2, creator=dummy_user
        ).name
        == name
    )

    assert record_1.last_modified != prev_timestamp_1
    assert record_2.last_modified != prev_timestamp_2
    assert record_1.revisions.count() == 2
    assert record_2.revisions.count() == 2

    prev_timestamp_1 = record_1.last_modified
    prev_timestamp_2 = record_2.last_modified

    with pytest.raises(ValueError) as e:
        create_record_link(
            name=name, record_from=record_1, record_to=record_2, creator=dummy_user
        )
        assert str(e) == "Link already exists."

    assert record_1.last_modified == prev_timestamp_1
    assert record_2.last_modified == prev_timestamp_2
    assert record_1.revisions.count() == 2
    assert record_2.revisions.count() == 2


def test_create_record_links(dummy_user, new_record, new_user):
    """Test if multiple record links are created correctly."""
    record_1 = new_record()
    record_2 = new_record()

    name = term = "test"
    prev_timestamp_1 = record_1.last_modified
    prev_timestamp_2 = record_2.last_modified

    # No permission.
    assert (
        create_record_links(
            record_1,
            [
                {"direction": "out", "record": record_2.id, "name": name, "term": term},
            ],
            creator=new_user(),
        )
        == 0
    )

    assert record_1.last_modified == prev_timestamp_1
    assert record_2.last_modified == prev_timestamp_2
    assert record_1.revisions.count() == 1
    assert record_2.revisions.count() == 1

    # Two valid links and three invalid links.
    assert (
        create_record_links(
            record_1,
            [
                {"direction": "out", "record": record_2.id, "name": name, "term": term},
                {"direction": "in", "record": record_2.id, "name": name, "term": term},
                # Duplicate link.
                {"direction": "out", "record": record_2.id, "name": name, "term": term},
                # Link with itself.
                {"direction": "out", "record": record_1.id, "name": name, "term": term},
                # Invalid record ID.
                {
                    "direction": "out",
                    "record": record_2.id + 1,
                    "name": name,
                    "term": term,
                },
            ],
            creator=dummy_user,
        )
        == 2
    )

    assert record_1.last_modified != prev_timestamp_1
    assert record_2.last_modified != prev_timestamp_2
    assert record_1.revisions.count() == 2
    assert record_2.revisions.count() == 2


def test_update_record_link(dummy_user, new_record, new_user):
    """Test if record links are updated correctly."""
    record_1 = new_record()
    record_2 = new_record()
    record_3 = new_record()

    record_link = create_record_link(
        name="test", record_from=record_1, record_to=record_2, creator=dummy_user
    )
    create_record_link(
        name="test2", record_from=record_1, record_to=record_2, creator=dummy_user
    )
    create_record_link(
        name="test2", record_from=record_1, record_to=record_3, creator=dummy_user
    )

    with pytest.raises(KadiPermissionError):
        update_record_link(record_link, name="test2", user=new_user())

    with pytest.raises(ValueError) as e:
        update_record_link(record_link, name="test2", user=dummy_user)
        assert str(e) == "Link already exists."

    with pytest.raises(ValueError) as e:
        update_record_link(
            record_link, name="test2", record_to=record_3.id, user=dummy_user
        )
        assert str(e) == "Link already exists."

    with pytest.raises(ValueError) as e:
        update_record_link(record_link, record_to=record_1.id, user=dummy_user)
        assert str(e) == "Cannot link record with itself."

    assert record_1.revisions.count() == 4
    assert record_2.revisions.count() == 3
    assert record_3.revisions.count() == 2

    # Perform an update with the same name and records.
    prev_timestamp_1 = record_1.last_modified
    prev_timestamp_2 = record_2.last_modified

    update_record_link(
        record_link,
        name="test",
        record_from=record_1.id,
        record_to=record_2.id,
        user=dummy_user,
    )

    assert record_1.last_modified == prev_timestamp_1
    assert record_2.last_modified == prev_timestamp_2
    assert record_1.revisions.count() == 4
    assert record_2.revisions.count() == 3

    # Perform an update with a new name.
    update_record_link(record_link, name="test3", user=dummy_user)

    assert record_link.name == "test3"
    assert record_1.last_modified != prev_timestamp_1
    assert record_2.last_modified != prev_timestamp_2
    assert record_1.revisions.count() == 5
    assert record_2.revisions.count() == 4

    # Perform an update with a new record.
    prev_timestamp_1 = record_1.last_modified
    prev_timestamp_2 = record_2.last_modified
    prev_timestamp_3 = record_3.last_modified

    update_record_link(record_link, record_to=record_3.id, user=dummy_user)

    assert record_link.record_from == record_1
    assert record_link.record_to == record_3
    assert record_1.last_modified != prev_timestamp_1
    assert record_2.last_modified != prev_timestamp_2
    assert record_3.last_modified != prev_timestamp_3
    assert record_1.revisions.count() == 6
    assert record_2.revisions.count() == 5
    assert record_3.revisions.count() == 3


def test_remove_record_link(dummy_user, new_record, new_user):
    """Test if record links are removed correctly."""
    record_1 = new_record()
    record_2 = new_record()

    record_link = create_record_link(
        name="test", record_from=record_1, record_to=record_2, creator=dummy_user
    )

    prev_timestamp_1 = record_1.last_modified
    prev_timestamp_2 = record_2.last_modified

    with pytest.raises(KadiPermissionError):
        remove_record_link(record_link, user=new_user())

    assert record_1.last_modified == prev_timestamp_1
    assert record_2.last_modified == prev_timestamp_2
    assert record_1.revisions.count() == 2
    assert record_2.revisions.count() == 2

    remove_record_link(record_link, user=dummy_user)

    assert not RecordLink.query.all()
    assert record_1.last_modified != prev_timestamp_1
    assert record_2.last_modified != prev_timestamp_2
    assert record_1.revisions.count() == 3
    assert record_2.revisions.count() == 3


def test_get_linked_record(dummy_record, dummy_user, new_record):
    """Test if linked records are retrieved correctly."""
    record = new_record()
    record_link = create_record_link(
        name="test", record_from=dummy_record, record_to=record, creator=dummy_user
    )

    assert not get_linked_record(record_link, new_record())
    assert get_linked_record(record_link, record) == dummy_record
    assert get_linked_record(record_link, dummy_record) == record


def test_get_permitted_record_links(dummy_record, dummy_user, new_record, new_user):
    """Test if permitted record links are determined correctly."""
    user = new_user()

    # Link a new record with the dummy record, but give the new user only access to the
    # dummy record.
    record_link = create_record_link(
        name="test",
        record_from=dummy_record,
        record_to=new_record(),
        creator=dummy_user,
    )
    add_role(user, "record", dummy_record.id, "member")

    assert (
        get_permitted_record_links(dummy_record, direction="out", user=dummy_user).one()
        == record_link
    )
    assert not get_permitted_record_links(
        dummy_record, direction="in", user=dummy_user
    ).all()
    assert not get_permitted_record_links(
        dummy_record, direction="out", user=user
    ).all()


def test_get_record_link_changes(dummy_record, dummy_user, new_file, new_record):
    """Test if changes of linked records are determined correctly."""
    record = new_record()
    record_link = create_record_link(
        name="test", record_from=dummy_record, record_to=record, creator=dummy_user
    )

    initial_changes = {
        dummy_record.id: {
            "record": {"count": 0, "revision": None},
            "files": {"count": 0},
        },
        record.id: {
            "record": {"count": 0, "revision": None},
            "files": {"count": 0},
        },
    }

    # Initial changes.
    changes = get_record_link_changes(record_link)

    assert changes == initial_changes

    # Updating a record and adding a file.
    update_record(dummy_record, identifier="test", user=dummy_user)
    new_file(record=record)

    changes = get_record_link_changes(record_link)

    assert changes == {
        dummy_record.id: {
            "record": {
                "count": 1,
                "revision": dummy_record.ordered_revisions.first().parent,
            },
            "files": {"count": 0},
        },
        record.id: {
            "record": {"count": 0, "revision": None},
            "files": {"count": 1},
        },
    }

    # Updating the record link.
    update_record_link(record_link, name="test2", user=dummy_user)
    changes = get_record_link_changes(record_link)

    assert changes == initial_changes


def test_get_record_links_graph(dummy_record, dummy_user, new_record):
    """Test if record link graph data is generated correctly."""
    record = new_record()
    record_link = create_record_link(
        name="test", record_from=dummy_record, record_to=record, creator=dummy_user
    )

    data = get_record_links_graph(dummy_record, user=dummy_user)

    records = data["records"]

    assert len(records) == 2
    assert records[0]["id"] == dummy_record.id
    assert records[1]["id"] == record.id

    record_links = data["record_links"]

    assert len(record_links) == 1
    assert record_links[0]["id"] == record_link.id
