# Copyright 2024 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

from flask import json

import kadi.lib.constants as const
from kadi.modules.templates.models import TemplateType
from kadi.modules.templates.utils import parse_import_data
from tests.constants import DEFAULT_RECORD_TEMPLATE_DATA
from tests.constants import DUMMY_EXTRA_NO_VALUE
from tests.constants import DUMMY_EXTRA_VALUE


def _encode_import_data(data):
    return BytesIO(json.dumps(data).encode())


def test_parse_import_data_json(new_record, new_template):
    """Test if JSON import data is parsed correctly."""

    # Test parsing record template data.
    template_data = {
        "id": 123,
        "type": TemplateType.RECORD,
        "identifier": "test",
        "data": {
            "identifier": "test2",
            "extras": [DUMMY_EXTRA_VALUE],
        },
    }
    data = _encode_import_data(template_data)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.RECORD) == {
        **template_data,
        "data": {
            **DEFAULT_RECORD_TEMPLATE_DATA,
            "identifier": template_data["data"]["identifier"],
            "extras": template_data["data"]["extras"],
        },
    }

    data.seek(0)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.EXTRAS) == {
        **template_data,
        "data": template_data["data"]["extras"],
    }

    # Test parsing extras template data.
    template_data = {
        "id": 123,
        "type": TemplateType.EXTRAS,
        "identifier": "test",
        "data": [DUMMY_EXTRA_VALUE],
    }
    data = _encode_import_data(template_data)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.RECORD) == {
        **template_data,
        "data": {"extras": template_data["data"]},
    }

    data.seek(0)

    assert (
        parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.EXTRAS)
        == template_data
    )

    # Test parsing record data.
    record_data = {
        "id": 123,
        "identifier": "test",
        "type": "test",
        "extras": [DUMMY_EXTRA_VALUE],
    }
    data = _encode_import_data(record_data)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.RECORD) == {
        "data": {**record_data, "extras": [DUMMY_EXTRA_NO_VALUE]}
    }

    data.seek(0)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON, TemplateType.EXTRAS) == {
        "data": [DUMMY_EXTRA_NO_VALUE]
    }
