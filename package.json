{
  "name": "kadi",
  "description": "Karlsruhe Data Infrastructure for Materials Science",
  "author": "Karlsruhe Institute of Technology",
  "private": true,
  "type": "module",
  "scripts": {
    "audit": "audit-ci --config audit-ci.json",
    "build": "webpack build --mode production",
    "dev": "webpack build --mode development",
    "eslint": "eslint --ext .js,.vue,.html --fix",
    "i18n": "i18next-scanner --config i18next-scanner.cjs",
    "watch": "webpack watch --mode development"
  },
  "postcss": {
    "plugins": {
      "autoprefixer": {}
    }
  },
  "browserslist": [
    "defaults"
  ],
  "dependencies": {
    "@codemirror/commands": "^6.5.0",
    "@codemirror/language": "^6.10.1",
    "@codemirror/state": "^6.4.1",
    "@codemirror/view": "^6.26.3",
    "@floating-ui/dom": "^1.6.3",
    "@fortawesome/fontawesome-free": "^6.5.2",
    "@kitware/vtk.js": "^30.4.0",
    "@ttskch/select2-bootstrap4-theme": "^1.5.2",
    "audit-ci": "^6.6.1",
    "autoprefixer": "^10.4.19",
    "axios": "^1.6.8",
    "bootstrap": "^4.6.2",
    "bootswatch": "^4.6.2",
    "clean-webpack-plugin": "^4.0.0",
    "css-loader": "^7.1.1",
    "d3": "^7.9.0",
    "dayjs": "^1.11.10",
    "detect-indent": "^7.0.1",
    "detect-newline": "^4.0.1",
    "diff": "^5.2.0",
    "eslint": "^8.57.0",
    "eslint-plugin-html": "^8.1.0",
    "eslint-plugin-vue": "^9.25.0",
    "flatpickr": "4.6.9",
    "glob": "^10.3.12",
    "i18next": "^23.11.2",
    "i18next-scanner": "^4.4.0",
    "jquery": "^3.7.1",
    "js-cookie": "^3.0.5",
    "katex": "^0.16.10",
    "lato-font": "^3.0.0",
    "markdown-it": "^14.1.0",
    "markdown-it-sub": "^2.0.0",
    "markdown-it-sup": "^2.0.0",
    "markdown-it-texmath": "^1.0.0",
    "mini-css-extract-plugin": "^2.9.0",
    "popper.js": "^1.16.1",
    "postcss-load-config": "^5.0.3",
    "postcss-loader": "^8.1.1",
    "regenerator-runtime": "^0.14.1",
    "resolve-url-loader": "^5.0.0",
    "rete": "^1.5.2",
    "rete-area-plugin": "^0.2.1",
    "rete-connection-plugin": "^0.9.0",
    "rete-context-menu-plugin": "^0.6.0",
    "rete-vue-render-plugin": "^0.5.2",
    "sass": "~1.61.0",
    "sass-loader": "^14.2.1",
    "select2": "^4.0.13",
    "shepherd.js": "^11.2.0",
    "three": "^0.163.0",
    "vue": "^2.7.16",
    "vue-loader": "^15.11.1",
    "vue-template-compiler": "^2.7.16",
    "vuedraggable": "^2.24.3",
    "webpack": "^5.91.0",
    "webpack-cli": "^5.1.4",
    "xlsx": "https://cdn.sheetjs.com/xlsx-0.20.2/xlsx-0.20.2.tgz",
    "zxcvbn": "^4.4.2"
  },
  "eslintConfig": {
    "env": {
      "browser": true,
      "es2021": true,
      "jquery": true
    },
    "extends": [
      "eslint:recommended",
      "plugin:vue/essential"
    ],
    "globals": {
      "kadi": "readonly",
      "$t": "readonly",
      "axios": "readonly",
      "dayjs": "readonly",
      "i18next": "readonly",
      "Vue": "readonly"
    },
    "ignorePatterns": [
      "kadi/static",
      "kadi/templates/base.html"
    ],
    "parserOptions": {
      "ecmaVersion": 2021,
      "sourceType": "module"
    },
    "plugins": [
      "html",
      "vue"
    ],
    "rules": {
      "accessor-pairs": "error",
      "array-bracket-newline": "error",
      "array-bracket-spacing": "error",
      "array-callback-return": "error",
      "arrow-parens": "error",
      "arrow-spacing": "error",
      "block-scoped-var": "error",
      "block-spacing": "error",
      "brace-style": "error",
      "camelcase": [
        "error",
        {
          "properties": "never"
        }
      ],
      "comma-dangle": [
        "error",
        "always-multiline"
      ],
      "comma-spacing": "error",
      "comma-style": "error",
      "computed-property-spacing": "error",
      "consistent-return": "error",
      "curly": "error",
      "default-case": "error",
      "default-case-last": "error",
      "default-param-last": "error",
      "dot-location": [
        "error",
        "property"
      ],
      "dot-notation": "error",
      "eol-last": "error",
      "eqeqeq": "error",
      "func-call-spacing": "error",
      "func-name-matching": "error",
      "func-style": [
        "error",
        "declaration",
        {
          "allowArrowFunctions": true
        }
      ],
      "function-call-argument-newline": [
        "error",
        "consistent"
      ],
      "function-paren-newline": "error",
      "generator-star-spacing": "error",
      "grouped-accessor-pairs": "error",
      "implicit-arrow-linebreak": "error",
      "indent": [
        "error",
        2,
        {
          "SwitchCase": 1
        }
      ],
      "init-declarations": "error",
      "key-spacing": "error",
      "keyword-spacing": "error",
      "linebreak-style": "error",
      "lines-between-class-members": "error",
      "max-len": [
        "error",
        {
          "code": 120
        }
      ],
      "new-cap": "error",
      "new-parens": "error",
      "no-array-constructor": "error",
      "no-await-in-loop": "error",
      "no-bitwise": "error",
      "no-caller": "error",
      "no-confusing-arrow": "error",
      "no-console": [
        "error",
        {
          "allow": [
            "warn",
            "error"
          ]
        }
      ],
      "no-constant-condition": [
        "error",
        {
          "checkLoops": false
        }
      ],
      "no-constructor-return": "error",
      "no-div-regex": "error",
      "no-duplicate-imports": "error",
      "no-else-return": "error",
      "no-empty-function": "error",
      "no-eq-null": "error",
      "no-eval": "error",
      "no-extend-native": "error",
      "no-extra-bind": "error",
      "no-extra-label": "error",
      "no-floating-decimal": "error",
      "no-implicit-coercion": "error",
      "no-implicit-globals": "error",
      "no-implied-eval": "error",
      "no-iterator": "error",
      "no-label-var": "error",
      "no-labels": "error",
      "no-lone-blocks": "error",
      "no-loop-func": "error",
      "no-loss-of-precision": "error",
      "no-mixed-operators": "error",
      "no-multi-spaces": "error",
      "no-multi-str": "error",
      "no-multiple-empty-lines": "error",
      "no-new-func": "error",
      "no-new-object": "error",
      "no-new-wrappers": "error",
      "no-octal-escape": "error",
      "no-param-reassign": "error",
      "no-proto": "error",
      "no-restricted-globals": "error",
      "no-restricted-imports": "error",
      "no-restricted-properties": "error",
      "no-return-await": "error",
      "no-script-url": "error",
      "no-self-compare": "error",
      "no-sequences": "error",
      "no-tabs": "error",
      "no-template-curly-in-string": "error",
      "no-throw-literal": "error",
      "no-trailing-spaces": "error",
      "no-undef-init": "error",
      "no-undefined": "error",
      "no-unmodified-loop-condition": "error",
      "no-unneeded-ternary": "error",
      "no-unused-expressions": "error",
      "no-unused-vars": "error",
      "no-use-before-define": "error",
      "no-useless-backreference": "error",
      "no-useless-call": "error",
      "no-useless-computed-key": "error",
      "no-useless-concat": "error",
      "no-useless-constructor": "error",
      "no-useless-rename": "error",
      "no-useless-return": "error",
      "no-var": "error",
      "no-void": "error",
      "no-whitespace-before-property": "error",
      "nonblock-statement-body-position": "error",
      "object-curly-newline": "error",
      "object-curly-spacing": "error",
      "object-shorthand": "error",
      "one-var-declaration-per-line": "error",
      "operator-assignment": "error",
      "operator-linebreak": [
        "error",
        "before"
      ],
      "padded-blocks": [
        "error",
        "never"
      ],
      "padding-line-between-statements": "error",
      "prefer-const": "error",
      "prefer-exponentiation-operator": "error",
      "prefer-numeric-literals": "error",
      "prefer-object-spread": "error",
      "prefer-promise-reject-errors": "error",
      "prefer-regex-literals": "error",
      "prefer-rest-params": "error",
      "prefer-spread": "error",
      "prefer-template": "error",
      "quotes": [
        "error",
        "single"
      ],
      "radix": "error",
      "require-await": "error",
      "rest-spread-spacing": [
        "error",
        "never"
      ],
      "semi": "error",
      "semi-spacing": "error",
      "semi-style": "error",
      "sort-vars": "error",
      "space-before-blocks": "error",
      "space-before-function-paren": [
        "error",
        "never"
      ],
      "space-in-parens": "error",
      "space-infix-ops": "error",
      "space-unary-ops": "error",
      "spaced-comment": "error",
      "switch-colon-spacing": "error",
      "symbol-description": "error",
      "template-curly-spacing": "error",
      "template-tag-spacing": "error",
      "unicode-bom": "error",
      "vars-on-top": "error",
      "wrap-iife": "error",
      "wrap-regex": "error",
      "yield-star-spacing": "error",
      "yoda": "error",
      "vue/attribute-hyphenation": "error",
      "vue/component-definition-name-casing": "error",
      "vue/component-name-in-template-casing": [
        "error",
        "kebab-case"
      ],
      "vue/html-closing-bracket-spacing": "error",
      "vue/html-end-tags": "error",
      "vue/html-indent": "error",
      "vue/html-quotes": "error",
      "vue/match-component-file-name": "error",
      "vue/multi-word-component-names": "off",
      "vue/mustache-interpolation-spacing": "error",
      "vue/no-multi-spaces": "error",
      "vue/no-spaces-around-equal-signs-in-attribute": "error",
      "vue/no-template-shadow": "error",
      "vue/padding-line-between-blocks": "error",
      "vue/prop-name-casing": "error",
      "vue/require-direct-export": "error",
      "vue/require-prop-types": "error",
      "vue/this-in-template": "error",
      "vue/v-bind-style": "error",
      "vue/v-on-style": "error",
      "vue/v-on-function-call": "error",
      "vue/v-slot-style": "error"
    }
  }
}
